<!DOCTYPE html>
<html>
<head>
    <title>[ORÇAMENTO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Empresa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $empresa }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Prefere contato via:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $contato }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Produtos:</span><br><span style='color:#000;font-size:14px;font-family:Verdana;'>{!! $produtos !!}</span>
@if($observacoes)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Observações:</span><br><span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $observacoes }}</span>
@endif
</body>
</html>
