<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.chamadas*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.chamadas.index') }}">Chamadas</a>
    </li>
    <li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.empresa.index') }}">Empresa</a>
    </li>
	<li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.produtos.index') }}">Produtos</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li @if(str_is('painel.orcamentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.orcamentos.index') }}">
            Orçamentos
            @if($orcamentosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $orcamentosNaoLidos }}</span>
            @endif
        </a>
    </li>
</ul>
