@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Orçamentos</h2>
    </legend>

    @if(!count($orcamentos))
    <div class="alert alert-warning" role="alert">Nenhum orçamento recebido.</div>
    @else
    <table class="table table-striped table-bordered table-hover data-table">
        <thead>
            <tr>
                <th>Data</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Empresa</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($orcamentos as $orcamento)

            <tr class="tr-row @if(!$orcamento->lido) warning @endif">
                <td data-order="{{ $orcamento->created_at_order }}">{{ $orcamento->created_at }}</td>
                <td>{{ $orcamento->nome }}</td>
                <td>
                    <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $orcamento->email }}" style="margin-right:5px;border:0;transition:background .3s">
                        <span class="glyphicon glyphicon-copy"></span>
                    </button>
                    {{ $orcamento->email }}
                </td>
                <td>{{ $orcamento->telefone }}</td>
                <td>{{ $orcamento->empresa }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.orcamentos.destroy', $orcamento->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.orcamentos.show', $orcamento->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ler orçamento
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

                        <a href="{{ route('painel.orcamentos.toggle', $orcamento->id) }}" class="btn btn-sm pull-left {{ ($orcamento->lido ? 'btn-warning' : 'btn-success') }}">
                            <span class="glyphicon small {{ ($orcamento->lido ? 'glyphicon-repeat' : 'glyphicon-ok') }}"></span>
                        </a>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @endif

@stop
