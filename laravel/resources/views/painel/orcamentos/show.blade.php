@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Orçamentos</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $orcamento->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $orcamento->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $orcamento->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $orcamento->email }}
        </div>
    </div>

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $orcamento->telefone }}</div>
    </div>

    <div class="form-group">
        <label>Empresa</label>
        <div class="well">{{ $orcamento->empresa }}</div>
    </div>

    <div class="form-group">
        <label>Prefere contato via</label>
        <div class="well">{{ $orcamento->contato }}</div>
    </div>

    <div class="form-group">
        <label>Produtos</label>
        <div class="well">{!! $orcamento->produtos !!}</div>
    </div>

@if($orcamento->observacoes)
    <div class="form-group">
        <label>Observações</label>
        <div class="well">{{ $orcamento->observacoes }}</div>
    </div>
@endif

    <a href="{{ route('painel.orcamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
