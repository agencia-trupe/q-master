@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('linha', 'Linha') !!}
    {!! Form::text('linha', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'produtos']) !!}
</div>

<div class="well form-group">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('catalogo', 'Catálogo') !!}
                @if(isset($registro) && $registro->catalogo)
                <p>
                    <a href="{{ url('assets/catalogos/'.$registro->catalogo) }}" target="_blank" style="display:block;">{{ $registro->catalogo }}</a>
                    <a href="{{ route('painel.produtos.deleteCatalogo', $registro->id) }}" class="btn-delete btn-delete-link label label-danger">
                        <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
                        REMOVER
                    </a>
                </p>
                @endif
                {!! Form::file('catalogo', ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group" style="margin-bottom: 0">
                {!! Form::label('catalogo_nome', 'Nome do Catálogo') !!}
                {!! Form::text('catalogo_nome', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
