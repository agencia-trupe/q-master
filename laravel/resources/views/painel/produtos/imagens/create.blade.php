@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo }} /</small> Adicionar Imagem</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.imagens.store', $produto->id], 'files' => true]) !!}

        @include('painel.produtos.imagens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
