@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo }} /</small> Editar Imagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.imagens.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.imagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
