@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('chamada', 'Chamada') !!}
    {!! Form::text('chamada', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('box_1_titulo', 'Box 1 Título') !!}
            {!! Form::text('box_1_titulo', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('box_1_texto', 'Box 1 Texto') !!}
            {!! Form::textarea('box_1_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('box_2_titulo', 'Box 2 Título') !!}
            {!! Form::text('box_2_titulo', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('box_2_texto', 'Box 2 Texto') !!}
            {!! Form::textarea('box_2_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('box_3_titulo', 'Box 3 Título') !!}
            {!! Form::text('box_3_titulo', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('box_3_texto', 'Box 3 Texto') !!}
            {!! Form::textarea('box_3_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem 1 (630 x 355px)') !!}
    <img src="{{ url('assets/img/empresa/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2 (288 x 215px)') !!}
            <img src="{{ url('assets/img/empresa/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3 (160 x 215px)') !!}
            <img src="{{ url('assets/img/empresa/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4 (160 x 215px)') !!}
            <img src="{{ url('assets/img/empresa/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
