@extends('frontend.common.template')

@section('content')

    <div class="main empresa">
        <div class="textos">
            <div class="chamada">
                <div class="center">
                    <h1>A Q&middot;Master</h1>
                    <h2>{{ $empresa->chamada }}</h2>
                </div>
            </div>
            <div class="center">
                {!! $empresa->texto !!}
            </div>
        </div>

        <div class="imagens">
            <div class="center">
                <div class="imagens-wrapper">
                    @foreach(range(1,4) as $i)
                    <img src="{{ asset('assets/img/empresa/'.$empresa->{'imagem_'.$i}) }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>

        <div class="center">
            <div class="boxes">
                @foreach(range(1,3) as $i)
                <div class="box">
                    <h3>{{ $empresa->{'box_'.$i.'_titulo'} }}</h3>
                    {!! $empresa->{'box_'.$i.'_texto'} !!}
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
