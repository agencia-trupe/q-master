    <footer>
        <div class="center">
            <div class="col">
                <a href="{{ route('home') }}">HOME</a>
                <a href="{{ route('empresa') }}">EMPRESA</a>
                <a href="{{ route('orcamento') }}">ORÇAMENTO</a>
            </div>
            <div class="col">
                <a href="{{ route('produtos') }}">PRODUTOS</a>
                <div class="lista">
                    @foreach($produtosCategoriasFooter as $categoria)
                    <a href="{{ route('produtos', $categoria->slug) }}">
                        {{ $categoria->titulo }}
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="col">
                <a href="{{ route('contato') }}">CONTATO</a>
                <div class="contato-div">
                    <p class="telefone">
                        @foreach(explode(',', trim($contato->telefones)) as $telefone)
                        <span>{{ $telefone }}</span>
                        @endforeach
                    </p>
                    <p class="whatsapp">{{ $contato->whatsapp }}</p>
                </div>
                <div class="contato-div">
                    <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                    <p class="endereco">{!! $contato->endereco !!}</p>
                </div>
            </div>
            <div class="col copyright">
                <img src="{{ asset('assets/img/layout/logo-rodape.png') }}" alt="">
                <p>
                    © {{ date('Y') }} Q-Master Equipamentos Elétricos Ltda<br>
                    Todos os direitos reservados.<br><br>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
