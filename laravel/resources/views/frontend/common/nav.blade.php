<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>Home</a>
<a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>Empresa</a>
<a href="{{ route('produtos') }}" @if(Tools::isActive('produtos')) class="active" @endif>Produtos</a>
<a href="{{ route('orcamento') }}" @if(Tools::isActive('orcamento')) class="active" @endif>Orçamento</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>Contato</a>
