    <header @if(Route::currentRouteName() == 'home' || Route::currentRouteName() == 'empresa') class="home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
            <nav class="nav-desktop">
                @include('frontend.common.nav')
            </nav>
            <nav class="nav-secundaria">
                <span class="telefone">{{ explode(',', trim($contato->telefones))[0] }}</span>
                <span class="whatsapp">{{ $contato->whatsapp }}</span>
                @if($contato->facebook)
                    <a href="{{ $contato->facebook }}" class="facebook" target="_blank"></a>
                @endif
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>

    <nav class="nav-mobile" style="z-index:9997">
        @include('frontend.common.nav')
    </nav>
