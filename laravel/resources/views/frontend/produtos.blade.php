@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <div class="detalhes">
                <div class="wrapper">
                    <h2>{{ $categoria->linha }}</h2>

                    @if(count($categoria->imagens))
                    <div class="imagens">
                        @foreach($categoria->imagens as $imagem)
                        <a href="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}" class="imagem-fancybox" rel="galeria" title="{{ $imagem->legenda }}">
                            <img src="{{ asset('assets/img/produtos/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                            @if($imagem->legenda)
                            <span>{{ $imagem->legenda }}</span>
                            @endif
                        </a>
                        @endforeach
                    </div>
                    @endif

                    <div class="texto">
                        {!! $categoria->descricao !!}
                    </div>
                </div>
            </div>

            <nav>
                <h2 class="titulo">PRODUTOS</h2>
                @foreach($categorias as $c)
                <a href="{{ route('produtos', $c->slug) }}" @if(isset($categoria) && $c->slug == $categoria->slug) class="active" @endif>{{ $c->titulo }}</a>
                @endforeach

                @if($categoria->catalogo)
                <a href="{{ route('catalogo', $categoria->id) }}" target="_blank" class="catalogo catalogo-popup">
                    <span>DOWNLOAD</span>
                    {{ $categoria->catalogo_nome }}
                </a>
                @endif
            </nav>
        </div>
    </div>

@endsection
