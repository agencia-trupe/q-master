@extends('frontend.common.template')

@section('content')

    <div class="main orcamento">
        <div class="center">
            <h2 class="titulo">ORÇAMENTO</h2>

            <form action="{{ route('orcamento.post') }}" id="form-orcamento" method="POST">
                <div class="left">
                    <div class="wrapper">
                        <p>Informe seus dados:</p>
                        <input type="text" name="nome" id="nome" placeholder="nome" required>
                        <input type="email" name="email" id="email" placeholder="e-mail" required>
                        <input type="text" name="telefone" id="telefone" placeholder="telefone" required>
                        <input type="text" name="empresa" id="empresa" placeholder="empresa" required>
                        <select name="contato" id="contato" required>
                            <option value="" disabled selected>prefere contato via: [selecione]</option>
                            <option value="e-mail">e-mail</option>
                            <option value="telefone">telefone</option>
                        </select>
                    </div>
                </div>

                <div class="right">
                    <div class="wrapper">
                        <p>Adicione produtos ao orçamento:</p>
                        <div class="orcamento-produtos"></div>
                        <div class="campos-produto">
                            <select name="produtos" id="produtos">
                                <option value="" selected>Produto [selecione...]</option>
                                @foreach($categorias as $categoria)
                                    <option value="{{ $categoria->titulo }}">
                                        {{ $categoria->titulo }}
                                    </option>
                                @endforeach
                            </select>
                            <input type="text" name="produto_codigo" id="produto_codigo" placeholder="Código">
                            <input type="text" name="produto_quantidade" id="produto_quantidade" placeholder="Quantidade">
                        <a href="#" class="btn-adiciona">ADICIONAR PRODUTO</a>

                        <textarea name="observacoes" id="observacoes" placeholder="Observações"></textarea>
                    </div>
                </div>

                <div id="form-orcamento-response"></div>
                <input type="submit" id="btn-envio" value="ENVIAR PEDIDO DE ORÇAMENTO">
            </form>
        </div>
    </div>

@endsection
