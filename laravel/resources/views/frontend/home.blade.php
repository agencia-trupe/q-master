@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner">
            <div class="center">
                <div class="banner-texto">
                    <div>
                        <h2>{{ $banner->titulo }}</h2>
                        <p>{!! $banner->descricao !!}</p>
                        <a href="{{ $banner->link }}">saber mais &raquo;</a>
                    </div>
                </div>
                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
            </div>
        </div>
        @endforeach
        <div class="cycle-pager" style="z-index:101"></div>
    </div>

    <div class="main chamadas">
        <div class="center">
            <h2>
                <span>Empresa metalúrgica, fabricante de</span>
                caixas, quadros e painéis para montagens elétricas
            </h2>
            <p>SAIBA MAIS SOBRE NOSSOS PRODUTOS:</p>
            <div class="chamadas-thumbs">
                @foreach($chamadas as $chamada)
                <a href="{{ $chamada->link }}">
                    <img src="{{ asset('assets/img/chamadas/'.$chamada->imagem) }}" alt="">
                    <span>{{ $chamada->titulo }}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
