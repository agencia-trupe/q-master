export default function Orcamento() {
    var $selectProdutos = $('select#produtos'),
        $listaProdutos  = $('.orcamento-produtos');

    $('.btn-adiciona').click(function(event) {
        event.preventDefault();

        if (!$selectProdutos.val()) return;
        if (!$('#produto_codigo').val()) {
            $('#produto_codigo').focus();
            return alert('Preencha o código do produto.');
        }
        if (!$('#produto_quantidade').val()) {
            $('#produto_quantidade').focus();
            return alert('Preencha a quantidade do produto.');
        }

        $listaProdutos.append(`
            <span>
                - ${$selectProdutos.val()} | código: ${$('#produto_codigo').val()} | quantidade: ${$('#produto_quantidade').val()}
                <a href="#" class="btn-remove">(remover)</a>
            </span>
        `);
        $selectProdutos.val($selectProdutos.find('>:first-child').val());
        $('#produto_codigo').val('');
        $('#produto_quantidade').val('');

        verificaBotao();
    });

    $('body').on('click', '.btn-remove', function(event) {
        event.preventDefault();

        $(this).parent().remove();
        verificaBotao();
    });

    var verificaBotao = function() {
        var $botaoEnvio = $('#btn-envio');

        if ($listaProdutos.find('span').length == 0) {
            $botaoEnvio.fadeOut();
        } else {
            $botaoEnvio.fadeIn();
        }
    };

    var envioOrcamento = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-orcamento-response');

        if ($form.hasClass('sending')) return false;

        if ($listaProdutos.find('span').length == 0) {
            return $response.fadeOut()
                            .text('Selecione produtos ao orçamento').fadeIn('slow');
        }

        var produtos = '';

        $listaProdutos.find('span').each(function(index, el) {
            produtos += `${$(el).text().replace('(remover)', '').trim()}`;
            if ($listaProdutos.find('span').length - 1 != index) {
                produtos += '<br>';
            }
        });

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/orcamento',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                empresa: $('#empresa').val(),
                contato: $('#contato').val(),
                produtos,
                observacoes: $('#observacoes').val()
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
                $listaProdutos.html('');
                $selectProdutos.val($selectProdutos.find('>:first-child').val());
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    $('#form-orcamento').on('submit', envioOrcamento);
};
