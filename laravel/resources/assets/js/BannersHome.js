export default function BannersHome() {
    var $wrapper = $('.banners');
    if (!$wrapper.length) return;

    $wrapper.cycle({
        slides: '>.banner',
        pagerTemplate: '<a href=#>{{slideNum}}</a>'
    });
};
