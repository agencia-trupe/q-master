import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import FormularioContato from './FormularioContato';
import BannersHome from './BannersHome';
import CatalogoPopup from './CatalogoPopup';
import ImagemLightbox from './ImagemLightbox';
import Orcamento from './Orcamento';

AjaxSetup();
MobileToggle();
FormularioContato();
BannersHome();
CatalogoPopup();
ImagemLightbox();
Orcamento();
