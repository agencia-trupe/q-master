<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chamada');
            $table->text('texto');
            $table->string('box_1_titulo');
            $table->text('box_1_texto');
            $table->string('box_2_titulo');
            $table->text('box_2_texto');
            $table->string('box_3_titulo');
            $table->text('box_3_texto');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }
}
