<?php

use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresa')->insert([
            'chamada' => '',
            'texto' => '',
            'box_1_titulo' => '',
            'box_1_texto' => '',
            'box_2_titulo' => '',
            'box_2_texto' => '',
            'box_3_titulo' => '',
            'box_3_texto' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
        ]);
    }
}
