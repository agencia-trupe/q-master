<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('orcamentosNaoLidos', \App\Models\Orcamento::naoLidos()->count());
        });
        view()->composer('frontend.common.*', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });
        view()->composer('frontend.common.footer', function($view) {
            $view->with('produtosCategoriasFooter', \App\Models\Categoria::ordenados()->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
