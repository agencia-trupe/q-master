<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Categoria;
use App\Models\Contato;

class ProdutosController extends Controller
{
    public function index(Categoria $categoria)
    {
        if (!$categoria->exists) {
            $categoria = Categoria::ordenados()->first();
        }

        if (!$categoria) abort('404');

        $categorias = Categoria::ordenados()->get();

        return view('frontend.produtos', compact('categorias', 'categoria'));
    }

    public function catalogo($id)
    {
        $categoria = Categoria::findOrFail($id);
        if (!$categoria->catalogo) abort('404');

        return view('frontend.catalogo', compact('categoria'));
    }

    public function catalogoPost(Request $request)
    {
        $this->validate($request, [
            'id'       => 'required',
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => 'required'
        ]);

        $categoria = Categoria::findOrFail($request->get('id'));
        if (!$categoria->catalogo) abort('404');

        $input = $request->all();
        $input['catalogo'] = $categoria->catalogo_nome;

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.catalogo', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CATÁLOGO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return response()->json([
            'link' => url('assets/catalogos/'.$categoria->catalogo)
        ]);
    }
}
