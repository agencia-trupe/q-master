<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Categoria;

class ProdutosController extends Controller
{
    public function index()
    {
        $registros = Categoria::ordenados()->get();

        return view('painel.produtos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.produtos.create');
    }

    public function store(ProdutosRequest $request)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('catalogo')) {
                $input['catalogo'] = Categoria::uploadFile();
            }

            Categoria::create($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Categoria $registro)
    {
        return view('painel.produtos.edit', compact('registro'));
    }

    public function update(ProdutosRequest $request, Categoria $registro)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('catalogo')) {
                $input['catalogo'] = Categoria::uploadFile();
            }

            $registro->update($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Categoria $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function deleteCatalogo($id)
    {
        try {

            $produto = Categoria::find($id);
            $produto->catalogo = null;
            $produto->save();

            return back()->with('success', 'Catálogo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir catálogo: '.$e->getMessage()]);

        }
    }

}
