<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Imagem;
use App\Models\Categoria;

class ProdutosImagensController extends Controller
{
    public function index(Categoria $produto)
    {
        $registros = Imagem::produto($produto->id)->ordenados()->get();

        return view('painel.produtos.imagens.index', compact('registros', 'produto'));
    }

    public function create(Categoria $produto)
    {
        return view('painel.produtos.imagens.create', compact('produto'));
    }

    public function store(Categoria $produto, ProdutosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['produto_id'] = $produto->id;

            if (isset($input['imagem'])) $input['imagem'] = Imagem::uploadImagem();

            Imagem::create($input);

            return redirect()->route('painel.produtos.imagens.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Categoria $produto, Imagem $registro)
    {
        return view('painel.produtos.imagens.edit', compact('registro', 'produto'));
    }

    public function update(ProdutosImagensRequest $request, Categoria $produto, Imagem $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Imagem::uploadImagem();

            $registro->update($input);

            return redirect()->route('painel.produtos.imagens.index', $produto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Categoria $produto, Imagem $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.imagens.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
