<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\OrcamentosRequest;

use App\Models\Categoria;
use App\Models\Orcamento;
use App\Models\Contato;

class OrcamentoController extends Controller
{
    public function index()
    {
        $categorias = Categoria::ordenados()->get();

        return view('frontend.orcamento', compact('categorias'));
    }

    public function post(OrcamentosRequest $request, Orcamento $orcamento)
    {
        $orcamento->create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.orcamento', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[ORÇAMENTO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Orçamento enviado com sucesso!'
        ];

        return response()->json($response);
    }
}
