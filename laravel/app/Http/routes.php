<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');
    Route::get('produtos/{categoria_slug?}', 'ProdutosController@index')->name('produtos');
    Route::get('catalogo/{id}', 'ProdutosController@catalogo')->name('catalogo');
    Route::post('catalogo', 'ProdutosController@catalogoPost')->name('catalogo.post');
    Route::get('orcamento', 'OrcamentoController@index')->name('orcamento');
    Route::post('orcamento', 'OrcamentoController@post')->name('orcamento.post');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::get('produtos/delete-catalogo/{produtos_id}', 'ProdutosController@deleteCatalogo')->name('painel.produtos.deleteCatalogo');
		Route::resource('produtos', 'ProdutosController');
        Route::resource('produtos.imagens', 'ProdutosImagensController');
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('chamadas', 'ChamadasController');
		Route::resource('banners', 'BannersController');

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::get('orcamentos/{orcamentos}/toggle', ['as' => 'painel.orcamentos.toggle', 'uses' => 'OrcamentosController@toggle']);
        Route::resource('orcamentos', 'OrcamentosController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
