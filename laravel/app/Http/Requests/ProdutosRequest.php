<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo'        => 'required',
            'linha'         => 'required',
            'descricao'     => 'required',
            'catalogo'      => 'mimes:pdf',
            'catalogo_nome' => 'required'
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
