<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chamada' => 'required',
            'texto' => 'required',
            'box_1_titulo' => 'required',
            'box_1_texto' => 'required',
            'box_2_titulo' => 'required',
            'box_2_texto' => 'required',
            'box_3_titulo' => 'required',
            'box_3_texto' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
        ];
    }
}
