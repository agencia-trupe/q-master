<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Imagem extends Model
{
    protected $table = 'produtos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 345,
                'height'  => 230,
                'bgcolor' => '#fff',
                'path'    => 'assets/img/produtos/imagens/thumbs/'
            ],
            [
                'width'   => 800,
                'height'  => null,
                'bgcolor' => '#fff',
                'path'    => 'assets/img/produtos/imagens/'
            ],
        ]);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\Categoria', 'produto_id');
    }
}
