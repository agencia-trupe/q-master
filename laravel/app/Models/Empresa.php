<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 630,
            'height' => 355,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 288,
            'height' => 215,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 160,
            'height' => 215,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 160,
            'height' => 215,
            'path'   => 'assets/img/empresa/'
        ]);
    }

}
